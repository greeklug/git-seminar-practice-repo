# Practical exercise

This is a practical exercise for the Git seminar. Read the instruction in the file `practical-training.pdf`.

_Summary_

- Create an account in Git lab.
- Request to the repository.
- Recreate the history of the example discusses in the presentations (see instruction file).
- Push your commits in branches prefix by your user name, e.g. gkaf/master, gkaf/configuration.
